$(document).ready(function () {
  $('div.imagefield-gallery-scrollbox').each(function() {
    var self = this;
    $('div.preview-display', self).css({ backgroundImage: 'url("' + $('div.thumbnail-browser', self).children().children().attr('href') + '")', backgroundPosition: 'center', backgroundRepeat: 'no-repeat'});
    $('div.title-box', self).html($('div.thumbnail-browser', self).children().children().children().attr('title'));
    var tbwidth = ($('div.thumbnail', self).width() + parseInt($('div.thumbnail', self).css('margin-left')) + parseInt($('div.thumbnail', self).css('border-left-width')) + parseInt($('div.thumbnail', self).css('border-right-width')))*$('div.thumbnail', self).length;
    $('div.thumbnail-browser', self).width(tbwidth);
    $('div.thumbnail a', self).click(function() {
      $('div.preview-display', self).css({ backgroundImage: 'url("' + $(this).attr('href') + '")', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', display: 'none'}).fadeIn('slow');
      $('div.title-box', self).html($(this).children().attr('title'));
      return false;
    });
    $('div.thumbnail-browser', self).offset();
    $('div.nav a.left', self).click(function(){
      $('div.thumbnail-browser', self).animate({left: '+='+($('div.thumbnail', self).width() + parseInt($('div.thumbnail', self).css('margin-left')) + parseInt($('div.thumbnail', self).css('border-left-width')) + parseInt($('div.thumbnail', self).css('border-right-width')))}, 500);
      return false;
    });
    $('div.nav a.right', self).click(function(){
      $('div.thumbnail-browser', self).animate({left: '-='+($('div.thumbnail', self).width() + parseInt($('div.thumbnail', self).css('margin-left')) + parseInt($('div.thumbnail', self).css('border-left-width')) + parseInt($('div.thumbnail', self).css('border-right-width')))}, 500);
      return false;
    });
    $('div.nav a.top', self).click(function(){
      $('div.thumbnail-browser', self).animate({top: '-='+($('div.thumbnail', self).height() + parseInt($('div.thumbnail', self).css('margin-top')) + parseInt($('div.thumbnail', self).css('border-top-width')) + parseInt($('div.thumbnail', self).css('border-bottom-width')))}, 500);
      return false;
    });
    $('div.nav a.bottom', self).click(function(){
      $('div.thumbnail-browser', self).animate({top: '+='+($('div.thumbnail', self).height() + parseInt($('div.thumbnail', self).css('margin-top')) + parseInt($('div.thumbnail', self).css('border-top-width')) + parseInt($('div.thumbnail', self).css('border-bottom-width')))}, 500);
      return false;
    });
  });
});